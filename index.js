let NginxConfFile = require('nginx-conf').NginxConfFile;
let NGINX_PATH = "C:/Program Files/nginx";

function expose(subdomain, port, isSecure, cert, cert_key, callback,shouldReload_nginx=true){
    // start_nginx();
    NginxConfFile.create(NGINX_PATH + '/conf/nginx.conf', function(err, conf){
        if(err){
            console.error("error ", err);
            return;
        }

        console.log(conf.nginx.http.server.length); //www www
        let line = conf.nginx.http.server.length;
        let protocol = isSecure ? 'https' : 'http';
        conf.nginx.http._add('server');
        conf.nginx.http.server[line]._add('listen', isSecure ? '443 ssl' : '80');
        conf.nginx.http.server[line]._add('server_name', subdomain);
        if(isSecure){
            conf.nginx.http.server[line]._add('ssl_certificate', cert);
            conf.nginx.http.server[line]._add('ssl_certificate_key', cert_key);
            conf.nginx.http.server[line]._add('ssl_session_cache', "shared:SSL:1m");
            conf.nginx.http.server[line]._add('ssl_session_timeout', "5m");
            conf.nginx.http.server[line]._add('ssl_ciphers', "HIGH:!aNULL:!MD5");
            conf.nginx.http.server[line]._add('ssl_prefer_server_ciphers', "on");
        }
        conf.nginx.http.server[line]._add('location', '/');
        conf.nginx.http.server[line].location._add('proxy_pass', protocol + '://127.0.0.1:'+port);
        conf.nginx.http.server[line].location._add('proxy_http_version', '1.1');
        conf.nginx.http.server[line].location._add('proxy_set_header', 'Upgrade $http_upgrade');
        conf.nginx.http.server[line].location._add('proxy_set_header', 'Connection "upgrade"');
        conf.nginx.http.server[line].location._add('proxy_read_timeout', '86400s');
        conf.flush();
        console.log("added nginx entry for: ", subdomain);
        if(shouldReload_nginx)
            reload_nginx();
    });
}



function expose1111(subdomain, port, isSecure, cert, cert_key, callback){
    // start_nginx();
    NginxConfFile.create(NGINX_PATH + '/conf/nginx.conf', function(err, conf)
    {
        if(err){
            console.error("error ", err);
            return;
        }

        // console.log("conf.nginx.http : "+conf.nginx.http); //www www
        console.log("conf.nginx for subdomain: "+subdomain); //www www




        // console.log("conf.nginx.http.server : "+conf.nginx.http.server); //www www
        console.log("conf.nginx.http.server.length : "+conf.nginx.http.server.length); //www www

        let line = conf.nginx.http.server.length;
        let protocol = isSecure ? 'https' : 'http';


        //adding a new block
        conf.nginx.http._add('server');

        conf.nginx.http.server[line]._add('listen', isSecure ? '443 ssl' : '80');
        // conf.nginx.http.server[0]._add('listen', isSecure ? '443 ssl' : '80');
        conf.nginx.http.server[line]._add('server_name', subdomain);
        if(isSecure){
            conf.nginx.http.server[line]._add('ssl_certificate', cert);
            conf.nginx.http.server[line]._add('ssl_certificate_key', cert_key);
            conf.nginx.http.server[line]._add('ssl_session_cache', "shared:SSL:1m");
            conf.nginx.http.server[line]._add('ssl_session_timeout', "5m");
            conf.nginx.http.server[line]._add('ssl_ciphers', "HIGH:!aNULL:!MD5");
            conf.nginx.http.server[line]._add('ssl_prefer_server_ciphers', "on");
        }
        conf.nginx.http.server[line]._add('location', '/');
        // conf.nginx.http.server[line].location._add('proxy_pass', protocol + '://127.0.0.1:'+port);
        //conf.nginx.http.server[line].location._add('proxy_http_version', '1.1');
        //conf.nginx.http.server[line].location._add('proxy_set_header', 'Upgrade $http_upgrade');
        //conf.nginx.http.server[line].location._add('proxy_set_header', 'Connection "upgrade"');
        conf.flush();
        console.log("added nginx entry for: ", subdomain);
        reload_nginx();
    });
}


function expose222(subdomain, port, isSecure, cert, cert_key, callback)
{
    // start_nginx();
    NginxConfFile.create(NGINX_PATH + '/conf/nginx.conf', function(err, conf)
    {
        if(err){
            console.error("error ", err);
            return;
        }







        console.log("conf.nginx.http.server.length :"+ conf.nginx.http.server.length); //www www
        console.log("conf.nginx.http.server :"+ conf.nginx.http.server); //www www

        //writing values
        //NginxConfFile.create() automatically sets up a sync, so that whenever
        //a value is changed, or a node is removed/added, the file gets updated
        //immediately

        conf.on('flushed', function() {
            console.log('expose: finished writing to disk');
        });

        //listen to the flushed event to determine when the new file has been flushed to disk
        //conf.nginx.events.connections._value = 1000;

        //don't write to disk when something changes
        //conf.die('/etc/nginx.conf');
        //conf.nginx.events.connections._value = 2000; //change remains local, not in /etc/nginx.conf

        //write to a different file
        // conf.live('/etc/nginx.conf.bak');





        let line = conf.nginx.http.server.length;
        let protocol = isSecure ? 'https' : 'http';


        //https://github.com/tmont/nginx-conf

        /*

	 //adding and removing directives
	  conf.nginx.http._add('add_header', 'Cache-Control max-age=315360000, public');
	  console.log(conf.nginx.http.add_header._value); //Cache-Control max-age=315360000, public

	  conf.nginx.http._add('add_header', 'X-Load-Balancer lb-01');
	  conf.nginx.http._add('add_header', 'X-Secure true');
	  console.log(conf.nginx.http.add_header[0]._value); //Cache-Control max-age=315360000, public
	  console.log(conf.nginx.http.add_header[1]._value); //X-Load-Balancer lb-01
	  console.log(conf.nginx.http.add_header[2]._value); //X-Secure true

	  conf.nginx.http._remove('add_header'); //removes add_header[0]
	  conf.nginx.http._remove('add_header', 1); //removes add_header[1]

	  //if there's only one directive with a name, it is always flattened into a property
	  console.log(conf.nginx.http.add_header._value); //X-Load-Balancer lb-01
	  console.log(conf.nginx.http.add_header[0]); //undefined
	 */




        //adding a new block
        conf.nginx.http._add('server');


        //conf.nginx.http.server._add('listen', '80');
        //that'll create something like this:
        /*
		  server {
			listen 80;
		  }
		*/


        //multiple blocks----  conf.nginx.http.server[XXXXXX]._add
        conf.nginx.http.server[line]._add('listen', isSecure ? '443 ssl' : '80');

        /*
		   server {
			 listen 80;
		   }
		   server {
			 listen 443;
		   }
		 */




        conf.nginx.http.server[line]._add('server_name', subdomain);




        if(isSecure)
        {
            conf.nginx.http.server[line]._add('ssl_certificate', cert);
            conf.nginx.http.server[line]._add('ssl_certificate_key', cert_key);
            conf.nginx.http.server[line]._add('ssl_session_cache', "shared:SSL:1m");
            conf.nginx.http.server[line]._add('ssl_session_timeout', "5m");
            conf.nginx.http.server[line]._add('ssl_ciphers', "HIGH:!aNULL:!MD5");
            conf.nginx.http.server[line]._add('ssl_prefer_server_ciphers', "on");
        }

        // blocks with values:
        conf.nginx.http.server[line]._add('location', '/');
        conf.nginx.http.server[line].location._add('proxy_pass', protocol + '://127.0.0.1:'+port);
        conf.nginx.http.server[line].location._add('proxy_http_version', '1.1');
        conf.nginx.http.server[line].location._add('proxy_set_header', 'Upgrade $http_upgrade');
        conf.nginx.http.server[line].location._add('proxy_set_header', 'Connection "upgrade"');

        /*
		   server {
			 location / {
			   root /var/www/example.com;
			 }
		   }
		 */








        //force the synchronization
        conf.flush();
        console.log("Flushed111");

        //reading values
        //console.log("conf.nginx.user._value : "+conf.nginx.user._value); //www www
        console.log("conf.nginx.http.server.listen._value : "+conf.nginx.http.server.listen._value); //one.example.com
        //if there is more than one directive in a scope (e.g. location), then
        //you access them via array index rather than straight property access
        console.log(conf.nginx.http.server.location[3].root._value); // /spool/www

        reload_nginx();
    });
}



function reload_nginx(){
    let exec = require('child_process').exec;
    exec('nginx -s reload', {
        cwd: NGINX_PATH
    }, function(error, stdout, stderr){
        console.log("NGINX RELOAD OP: ", error, stdout, stderr);
    })
}

function start_nginx(){
    let exec = require("child_process").exec;
    exec('nginx', {
        cwd: NGINX_PATH
    }, function(error, stdout, stderr){
        console.log("NGINX Start", error, stdout, stderr);
    })
}

function dexpose(subdomain, callback){
    NginxConfFile.create(NGINX_PATH + '/conf/nginx.conf', function(err, conf){
        if(err){
            console.error("error: ", err);
            return;
        }
    });
}

function resetAll(){
    let exec = require("child_process").exec;
    exec('del nginx.conf && copy nginx.conf.bak nginx.conf', {
        cwd: NGINX_PATH + '/conf'
    }, function(err, stdout, stderr){
        if(err){
            console.error("Failed to reset configuration file.");
        }
        console.error("PROCEED WITH CAUTION - NGINX CONFIGURATION RESET COMPLETE")
    })
}

module.exports.expose = expose;
module.exports.resetAll = resetAll;
module.exports.reload_nginx = reload_nginx;
module.exports.start_nginx = start_nginx;
module.exports.dexpose = dexpose;
